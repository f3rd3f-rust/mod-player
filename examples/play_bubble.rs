use std::env;
use mod_play::play_mod;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() == 1 {
        panic!("Filename must be provided")
    }
    let filename = &args[1];
    play_mod(filename);
}