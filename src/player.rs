use std::fs::File;
use std::io::BufReader;
use rodio::Source;

pub fn play_wav(filename: &str) {
    let device = rodio::output_devices().nth(1).unwrap();
    println!("{}", device.name());

    let file = File::open(filename).unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    rodio::play_raw(&device, source.convert_samples());
    
    loop {}
}