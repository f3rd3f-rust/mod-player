use std::env;
use hound;

pub fn create_wav(filename: &str) -> String {
    let spec = hound::WavSpec {
        channels: 2,
        sample_rate: 22050,
        bits_per_sample: 32,
        sample_format: hound::SampleFormat::Float,
    };

    let temp_file = get_temp_file();

    let mut writer = hound::WavWriter::create(&temp_file, spec).unwrap();
    println!("{}",filename);
    let song = mod_player::read_mod_file(filename);
    let mut player_state : mod_player::PlayerState = mod_player::PlayerState::new( 
        song.format.num_channels, spec.sample_rate );
    loop {
        let ( left, right ) = mod_player::next_sample(&song, &mut player_state);
        writer.write_sample(left);
        writer.write_sample(right);
        if player_state.song_has_ended || player_state.has_looped { 
            break;
        }
    }

    temp_file
}

fn get_temp_file() -> String {
    let mut temp_file : String;
    let temp_dir = env::var("TMPDIR").unwrap();

    temp_file = temp_dir;
    temp_file.push_str("song.wav");
    temp_file
}
