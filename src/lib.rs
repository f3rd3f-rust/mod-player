mod player;
mod creator;

pub fn play_mod(filename: &str) {
    let temp_audio = creator::create_wav(filename);
    player::play_wav(&temp_audio);
}